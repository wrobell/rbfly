# commands:
#
#     clean
#         remove build directories, rendered documentation, and other build
#         related artefacts
#     build-ext
#         build project's Python extensions
#     build-dist
#         build project artifacts to be published
#     build-publish
#         publish project artifacts
#     build-system-install
#         install project build system
#     venv
#         create Python virtual environment in `.venv` directory
#     venv-activate
#         activate Python virtual environment from `.venv` directory
#     check-code
#         run code quality analysis
#     check-type
#         run static type analysis
#     test
#         run unit tests
#     doc
#         generate documentation
#     doc-upload
#         upload documentation
#
# project configuration:
#
#   USER_SHELL
#       user shell to execute when activating virtual environment
#   SCRIPTS
#       list of scripts, which are part of the project
#   DOC_DEST 
#       where to upload the documentation

.PHONY: clean build-ext build-dist build-publish build-system-install \
	venv venv-activate check-code check-type test doc doc-upload

PROJECT = $(shell grep '^name' setup.cfg | awk -F= '{gsub(/ /, "", $$2); print $$2}')
VERSION = $(shell grep '^version' setup.cfg | awk -F= '{gsub(/ /, "", $$2); print $$2}')
MODULE = $(shell echo $(PROJECT) | sed 's/-//g')
PKG_INFO = $(shell echo $(PROJECT) | sed 's/-/_/g').egg-info/PKG-INFO
EGG = $(shell echo $(PROJECT) | sed 's/-/_/g')

export PYTHONPATH=.

RSYNC = rsync -cav \
	--exclude=\*~ --exclude=.\* \
	--delete-excluded --delete-after \
	--no-owner --no-group \
	--progress --stats

clean:
	rm -rf .stamp-*
	rm -rf $(PROJECT).egg-info
	rm -rf build/doc build/latex
	rm -rf .mypy_cache .pytest_cache .ruff_cache

build-ext:
	python setup.py build_ext --inplace

build-dist:
	python -m build -n -s -x

build-publish:
	twine upload -s dist/$(PROJECT)-$(VERSION).tar.gz

build-system-install:
	pip install -U $$(python -c "import tomllib as tl; print(' '.join(tl.load(open('pyproject.toml', 'rb'))['build-system']['requires']))")

venv:
	virtualenv .venv

venv-activate:
	@source .venv/bin/activate && exec zsh

check-code: .stamp-$(PROJECT)-$(VERSION)
	ruff check $(MODULE) $(SCRIPTS) tests_int
	cython-lint --no-pycodestyle $(SOURCE_CYTHON)
	bandit -c pyproject.toml -r $(MODULE) $(SCRIPTS)

check-type: .stamp-$(PROJECT)-$(VERSION)
	mypy --strict --scripts-are-modules --implicit-reexport $(MODULE) $(SCRIPTS) tests_int

test: .stamp-$(PROJECT)-$(VERSION)
	pytest

doc: .stamp-$(PROJECT)-$(VERSION) $(DOC_DEPENDENCIES)
	sphinx-build -a -D exclude_patterns=rbfly.rst -b html -t html doc build/doc
	sphinx-build -a -D root_doc=rbfly -D exclude_patterns=index.rst,performance.rst -b latex doc build/latex
	# process performance report separately or figures are not numbered
	sphinx-build -a -D root_doc=performance -D exclude_patterns=index.rst,rbfly.rst -b latex doc build/latex
	make -C build/latex
	cp build/latex/*.pdf build/doc

doc-upload:
	$(RSYNC) build/doc/ $(DOC_DEST)

.stamp-$(PROJECT)-$(VERSION): $(PKG_INFO)
	touch .stamp-$(PROJECT)-$(VERSION)

$(PKG_INFO): setup.cfg $(wildcard setup.py)
	python -m build -n -s -x

