License
=======
RbFly library is licensed under terms of `GPL license, version 3
<https://www.gnu.org/licenses/gpl-3.0.en.html>`_. As stated in the license,
there is no warranty, so any usage is on your own risk.

Proprietary License
-------------------
`Art System Engineering Limited <https://art-se.eu/>`_ company is providing
proprietary licenses for RbFly library.

This enables enterprises to integrate RbFly library into their proprietary
applications, and addresses concerns of the enterprises about free-software
licenses.

Details about the proprietary license can be found at
https://art-se.eu/licenses.html.

.. vim: sw=4:et:ai
