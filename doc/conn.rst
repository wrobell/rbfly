Connection URI
==============
The string specifying RabbitMQ Streams connection is URI in the format::

     rabbitmq-stream://[user[:password]@][host[:port][/vhost]]

or for TLS::

     rabbitmq-stream+tls://[user[:password]@][host[:port][/vhost]]

The description of the URI parts is as follows

user
    Name of an user connecting to RabbitMQ Streams broker.
password
    Password of the user.
host
    RabbitMQ Streams broker hostname, `localhost` by default.
port
    RabbitMQ Streams broker port, `5552` by default.
vhost
    Virtual host of RabbitMQ Streams broker, it is `/` (slash) by default.

Examples of valid URI values:

.. code-block:: none

    rabbitmq-stream://
    rabbitmq-stream://localhost
    rabbitmq-stream://localhost/
    rabbitmq-stream://good-rabbit:1552/carrot
    rabbitmq-stream://user@good-rabbit
    rabbitmq-stream://user:passta@good-rabbit/carrot

Examples of valid URI values for TLS connections:

.. code-block:: none

    rabbitmq-stream+tls://
    rabbitmq-stream+tls://localhost
    rabbitmq-stream+tls://localhost/
    rabbitmq-stream+tls://good-rabbit:1551/carrot
    rabbitmq-stream+tls://user@good-rabbit
    rabbitmq-stream+tls://user:passta@good-rabbit/carrot

.. vim: sw=4:et:ai
