Quick Start
===========
The example below demonstrates various RbFly library features, which can be
utilized in an application using `RabbitMQ Streams
<https://www.rabbitmq.com/streams.html>`_:

- creating RabbitMQ Streams client using connection URI
- creating a stream (`demo` coroutine)
- creating a stream publisher and publishing messages to a stream
  (`send_data` coroutine)
- subscribing to a stream and receiving messages from a stream
  (`receive_data` coroutine)
- the script continues to run if RabbitMQ Streams broker stops and starts
  again

.. literalinclude:: ../scripts/rbfly-demo
   :language: python
   :start-at: import asyncio
   :end-at: asyncio.run

The source code of the demo can be downloaded from `RbFly code repository
<https://gitlab.com/wrobell/rbfly/-/blob/master/scripts/rbfly-demo>`_.

The following sections of the documentation discuss the features of RbFly
library in more detail.

.. vim: sw=4:et:ai
