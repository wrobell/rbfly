Changelog
=========
Year 2024
---------
2024-10-17, ver. 0.10.0
^^^^^^^^^^^^^^^^^^^^^^^
- improve amqp message decoding
- fix bugs related to rabbitmq broker automatic reconnection
- rabbitmq 3.13 or later is required
- python 3.11 or later is required

2024-07-11, ver. 0.9.0
^^^^^^^^^^^^^^^^^^^^^^
- add support for stream message filtering
- improve reporting of errors of rabbitmq streams protocol
- add documentation topic about application concurrency

2024-05-21, ver. 0.8.4
^^^^^^^^^^^^^^^^^^^^^^
- remove use of deprecated python functions

2024-05-04, ver. 0.8.3
^^^^^^^^^^^^^^^^^^^^^^
- fix errors in `rbfly.streams.connection` decorator

2024-03-19, ver. 0.8.2
^^^^^^^^^^^^^^^^^^^^^^
- improve type annotations for `rbfly.streams.connection` decorator

2024-01-07, ver. 0.8.1
^^^^^^^^^^^^^^^^^^^^^^
- fix typo: `stream_messsage_ctx -> stream_message_ctx`

Year 2023
---------
2023-11-02, ver. 0.8.0
^^^^^^^^^^^^^^^^^^^^^^
- allow tls connections to rabbitmq streams broker
- publish performance test results for amqp 1.0 codecs
- flush batch publisher on exit from an asynchronous context manager of
  a publisher
- avoid publisher ids and subscriber ids overflow by reusing and
  controlling the ids

2023-09-15, ver. 0.7.3
^^^^^^^^^^^^^^^^^^^^^^
- add support for null values in amqp messages
- use cython 3.0.2 to build rbfly binary extensions

2023-07-09, ver. 0.7.2
^^^^^^^^^^^^^^^^^^^^^^
- fix binary only decoder of rabbitmq streams messages
- fix reading duplicate messages when re-subscribing to a rabbitmq stream

2023-06-25, ver. 0.7.1
^^^^^^^^^^^^^^^^^^^^^^
- fix reconnection to rabbitmq streams broker, which was failing due to
  hidden subscriber class attributes

2023-06-21, ver. 0.7.0
^^^^^^^^^^^^^^^^^^^^^^
- rbfly is beta software, now
- add support for deduplication of published messages
- add support for publishing messages with amqp application properties
- publish performance test results for message publishing
- include aiokafka and apache kafka in message publishing performance tests
- fix potential buffer overflow issues when decoding message publishing
  confirmations
- use cython 3.0.0b2 to build rbfly binary extensions
- rename ``PublisherBatch`` class as ``PublisherBatchFast`` class
- rename ``PublisherBatchMem`` class as ``PublisherBatchLimit`` class

2023-05-12, ver. 0.6.2
^^^^^^^^^^^^^^^^^^^^^^
- fix parsing of virtual host in rabbitmq streams uri, i.e. `/vhost` ->
  `vhost` but `/` remains as `/`

2023-03-18, ver. 0.6.1
^^^^^^^^^^^^^^^^^^^^^^
- fix message id value type; it is 64-bit, not 32-bit value

2023-02-04, ver. 0.6.0
^^^^^^^^^^^^^^^^^^^^^^
- fix various buffer overflow issues
- split batch of messages, and send multiple batches, when the batch is too
  big to fit into rabbitmq streams protocol frame
- increase initial credit value to 128; this improves performance of
  reading messages with rabbitmq 3.11.8; the possible memory footprint of
  400 MB to read messages still seems to be acceptable
- adapt to rabbitmq 3.11.8 changes related to the handling of stream
  subscription credit

Year 2022
---------
2022-12-22, ver. 0.5.6
^^^^^^^^^^^^^^^^^^^^^^
- fix processing of credit value of rabbitmq stream subscription, when
  messages were published in batches

2022-12-19, ver. 0.5.5
^^^^^^^^^^^^^^^^^^^^^^
- improve maintenance of rabbitmq stream's subscription credit

2022-11-23, ver. 0.5.4
^^^^^^^^^^^^^^^^^^^^^^
- fix authentication with rabbitmq streams broker (send password if
  specified, not username)

2022-11-11, ver. 0.5.3
^^^^^^^^^^^^^^^^^^^^^^
- use recursive definition for container amqp types, i.e. sequence can
  contain another sequence or a dictionary
- documentation improvements

2022-11-01, ver. 0.5.2
^^^^^^^^^^^^^^^^^^^^^^
- fix encoding of binary data in containers, i.e. list of elements having
  bytes type

2022-10-16, ver. 0.5.1
^^^^^^^^^^^^^^^^^^^^^^
- fix deadlock of rabbitmq streams protocol publish method when no
  messages are sent

2022-10-09, ver. 0.5.0
^^^^^^^^^^^^^^^^^^^^^^
- implement rabbitmq streams batching publisher with memory protection and
  concurrently working from multiple asynchronous coroutines
- send heartbeat if there is no communication from rabbitmq streams broker
  and restart connection if no response

2022-09-12, ver. 0.4.2
^^^^^^^^^^^^^^^^^^^^^^
- allow sending messages via a streams publisher from multiple coroutines
- improve rabbitmq streams connection decorator type annotations

2022-08-16, ver. 0.4.1
^^^^^^^^^^^^^^^^^^^^^^
- minor fixes post 0.4.0 release

2022-08-16, ver. 0.4.0
^^^^^^^^^^^^^^^^^^^^^^
- add support for encoding and decoding of timestamp and UUID values in
  amqp 1.0 codec
- add support for decoding of message annotations and application
  properties of amqp message
- fix amqp decoder when dealing with unsigned char values (i.e. size of
  a byte string)
- fix storing of rabbitmq streams timestamp in message context object

2022-05-23, ver. 0.3.0
^^^^^^^^^^^^^^^^^^^^^^
- api change: do not store stream offset value in stream subscription
  method to give more control over the action to a programmer
- implement rabbitmq streams client method to store stream offset value
- implement function to get message context for a received stream message

2022-05-13, ver. 0.2.0
^^^^^^^^^^^^^^^^^^^^^^
- change publisher reference name scheme to ``<hostname>/<pid>``
- allow to set publisher reference name when creating a stream publisher
- improve handling of rabbitmq streams disconnections
- increase sleep time when retrying stream subscription or rabbitmq streams
  broker reconnection (current sleep time is too aggressive with simple
  broker restart)

2022-04-13, ver. 0.1.1
^^^^^^^^^^^^^^^^^^^^^^
- fix handling of multiple disconnection requests, i.e. when multiple
  coroutines share a client
- properly propagate asynchronous generators via the connection decorator

2022-04-06, ver. 0.1.0
^^^^^^^^^^^^^^^^^^^^^^
- improvements to decoding of amqp messages
- fix compilation issues on macos and freebsd operating systems

2022-03-25, ver. 0.0.4
^^^^^^^^^^^^^^^^^^^^^^
- add support for value of type list in amqp messages

2022-03-24, ver. 0.0.3
^^^^^^^^^^^^^^^^^^^^^^
- add support for values of type boolean, integer, float, and dictionaries
  in amqp messages

2022-01-24, ver. 0.0.2
^^^^^^^^^^^^^^^^^^^^^^
- fix encoding of messages

2022-01-16, ver. 0.0.1
^^^^^^^^^^^^^^^^^^^^^^
- initial release

.. vim: sw=4:et:ai
