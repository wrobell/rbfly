#!/usr/bin/env python3
#
# rbfly - a library for RabbitMQ Streams using Python asyncio
#
# Copyright (C) 2021-2024 by Artur Wroblewski <wrobell@riseup.net>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

"""
Script to send number of messages to a RabbitMQ stream using `rstream`
project.
"""

import argparse
import asyncio
import logging
import uvloop
import time
import typing as tp
from functools import partial

import rbfly.streams as rbs

import rstream.connection
from rstream import Producer, AMQPMessage
from rstream.exceptions import StreamAlreadyExists

from rbfly_script_util import Printer, create_printer, parse_args

logger = logging.getLogger(__name__)

T = tp.TypeVar('T')
Sender = tp.Callable[[T, str, int, int], tp.Coroutine[tp.Any, tp.Any, None]]

# rstream use 1 second timeout when connecting to rabbitmq streams broker;
# this is no enough during performance tests; last checked for rstream
# ver. 0.8.1
rstream.connection.CONNECT_TIMEOUT = 60

async def publisher_on_confirm(
        event: asyncio.Event,
        confirm: rstream.ConfirmationStatus
) -> None:
    """
    Receive publisher confirmation.

    The discussion below applies to RabbitMQ 3.12.0, and `rstream` 0.8.1.

    The function is used in batch publisher performance tests. The function
    waits for the first message of a batch to be confirmed, and should wait
    for the last one. This is based on the examples in `rstream` readme
    file.

    The implementation of the function acts in favour of `rstream` library.
    RabbitMQ Broker sends confirmation for a batch of 100 messages in two
    tranches (see discussion here https://groups.google.com/g/rabbitmq-users/c/Gra4fVH5NNQ).
    Also, more complex solution would result in a small performance
    degradation, so we leave it at that.
    """
    assert confirm.is_confirmed
    event.set()

async def send_message_amqp(
        publisher: Producer, stream: str, count: int, batch_size: int
) -> None:
    assert batch_size == 0
    for i in range(count):
        msg = AMQPMessage(body='{:010d}'.format(i))
        await publisher.send_wait(stream, msg)

async def send_message_binary(
        publisher: Producer, stream: str, count: int, batch_size: int
) -> None:
    assert batch_size == 0
    for i in range(count):
        await publisher.send_wait(stream, '{:010d}'.format(i).encode())

async def send_batch_amqp(
        publisher: Producer, stream: str, count: int, batch_size: int
) -> None:
    assert batch_size > 1
    event = asyncio.Event()
    event.clear()
    on_confirm = partial(publisher_on_confirm, event)
    for _ in range(count):
        messages = [AMQPMessage(body='{:010d}'.format(j)) for j in range(batch_size)]
        await publisher.send_batch(
            stream, messages, on_publish_confirm=on_confirm  # type: ignore
        )
        # wait for publisher confirmation
        await event.wait()
        event.clear()

async def send_batch_binary(
        publisher: Producer, stream: str, count: int, batch_size: int
) -> None:
    assert batch_size > 1
    event = asyncio.Event()
    event.clear()
    on_confirm = partial(publisher_on_confirm, event)
    for _ in range(count):
        messages = ['{:010d}'.format(j).encode() for j in range(batch_size)]
        await publisher.send_batch(stream, messages, on_publish_confirm=on_confirm)
        # wait for publisher confirmation
        await event.wait()
        event.clear()

# ruff: noqa: PLR0913
async def send_data(
        client: rbs.StreamsClient,
        stream: str,
        count: int,
        *,
        sender: Sender[Producer]=send_message_amqp,
        batch_size: int=0,
        printer: Printer | None=None,
) -> None:

    assert printer is not None
    assert client._cinfo.username is not None
    assert client._cinfo.password is not None

    n = count // batch_size if batch_size else count

    # ruff: noqa: SLF001
    async with Producer(
        client._cinfo.host,
        username=client._cinfo.username,
        password=client._cinfo.password
    ) as publisher:

        try:
            await publisher.create_stream(stream)
        except StreamAlreadyExists:
            logger.info('stream exists: {}'.format(stream))

        ts = time.monotonic()
        await sender(publisher, stream, n, batch_size)
        te = time.monotonic()
        printer(ts, te)

parser = argparse.ArgumentParser()
parser.add_argument(
    '--verbose', action='store_true',
    help='explain what is being done'
)
parser.add_argument(
    '-c', '--count', type=int, default=10 ** 6,
    help='Number of messages to publish to the stream, has to be'
        ' multiply of batch size'
)
parser.add_argument(
    '-b', '--batch-size', type=int, default=0,
    help='Batch size - numnber of messages in a batch'
)
parser.add_argument(
    '-t', '--type', choices=['amqp', 'binary'], default='amqp',
    help='Type of message sent to the stream: AMQP or binary opaque data'
)
parser.add_argument(
    '-f', '--format', choices=['pretty', 'csv'], default='csv',
    help='Output format of performance report'
)
parser.add_argument('uri', help='RabbitMQ Streams broker URI')
parser.add_argument('stream', help='Stream name')

args = parse_args(parser)
client = rbs.streams_client(args.uri)

decision = {
    ('amqp', True): send_batch_amqp,
    ('amqp', False): send_message_amqp,
    ('binary', True): send_batch_binary,
    ('binary', False): send_message_binary,
}

sender = decision[args.type, args.batch_size > 1]
printer = create_printer(args, 'rstream')

uvloop.install()
send_f = send_data(
    client,
    args.stream,
    count=args.count,
    sender=sender,
    batch_size=args.batch_size,
    printer=printer,
)
asyncio.run(send_f)

# vim: sw=4:et:ai
