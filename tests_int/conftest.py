#
# rbfly - a library for RabbitMQ Streams using Python asyncio
#
# Copyright (C) 2021-2024 by Artur Wroblewski <wrobell@riseup.net>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

"""
Fixtures and support functions for RbFly integration tests.

Run the integration tests with command

    $ make test-integration

Note, that RabbitMQ Streams broker must be started on local host.
"""

import dataclasses as dtc
import logging
import os
import time
import typing as tp
from collections.abc import AsyncIterator
from contextlib import asynccontextmanager

import rbfly.streams as rbs
from rbfly.streams.protocol import RabbitMQStreamsProtocol
from rbfly.streams.util import partition

import pytest as pt
import pytest_asyncio as pta
from pytest_lazy_fixtures import lf as lazy_fixture

logger = logging.getLogger(__name__)

DEFAULT_STREAM_SMALL = 100
DEFAULT_STREAM_LARGE = int(1e4)
STREAM_NAME_SMALL_BATCH = 'rbfly.integration.small.batch'

T = tp.TypeVar('T', rbs.Publisher, rbs.PublisherBatchFast)

Client: tp.TypeAlias = rbs.StreamsClient
StreamInput: tp.TypeAlias = tp.Iterable[int]
StreamPublisher: tp.TypeAlias = AsyncIterator[T]
StreamCtx: tp.TypeAlias = AsyncIterator['Stream']

@dtc.dataclass(frozen=True)
class Stream:
    name: str
    messages: AsyncIterator[int]

def pytest_collection_modifyitems(items: tp.Any) -> None:
    """
    Set timeout for integration tests.
    """
    for item in items:
        if item.get_closest_marker('timeout') is None:
            item.add_marker(pt.mark.timeout(210))

def pt_lazy(name: str) -> Stream:
    """
    Create fixture of given name lazily.
    """
    return lazy_fixture(name)  # type: ignore

def bf_extract(ctx: rbs.MessageCtx) -> str:
    """
    Bloom filter extract function.
    """
    assert type(ctx.body) is int
    n = 10
    k = ctx.body
    if n <= k < n * 2:
        v = 'f001'
    elif n * 4 <= k < n * 5:
        v = 'f002'
    elif n * 6 <= k < n * 6 + 5:
        v = 'f003'
    elif n * 6 + 5 <= k < n * 7:
        v = 'f004'
    else:
        return str((k + 1) * 1000)
    return v

@pta.fixture
async def rbfly_client() -> AsyncIterator[Client]:
    """
    RbFly client test fixture.
    """
    uri = os.environ.get(
        'RBFLY_TEST_URI', 'rabbitmq-stream://guest:guest@localhost'
    )
    client = rbs.streams_client(uri)
    try:
        yield client
    finally:
        await client.disconnect()

@pta.fixture
async def rbfly_protocol(rbfly_client: Client) -> RabbitMQStreamsProtocol:
    """
    RbFly client protocol test fixture.
    """
    return await rbfly_client.get_protocol()

@pta.fixture
def start_time() -> float:
    """
    Test fixture for a start time of an unit test.

    Use it as first parameter of a test function to estimate the start time
    of an unit test.
    """
    return time.time()

@pta.fixture
@asynccontextmanager
async def stream_pub_no_batch(
        rbfly_client: Client
) -> StreamPublisher[rbs.Publisher]:
    """
    RbFly stream publisher fixture.

    Actions:

    - create stream
    - create and yield publisher to a test function
    - delete stream
    """
    stream = 'rbfly.integration.publisher.no-batch'
    ctx = _stream_publisher_create(rbfly_client, stream, rbs.Publisher)
    async with ctx as publisher:
        yield publisher

@pta.fixture
@asynccontextmanager
async def stream_pub_batch(
        rbfly_client: Client
) -> StreamPublisher[rbs.PublisherBatchFast]:
    """
    RbFly stream batch publisher fixture.

    Actions:

    - create stream
    - create and yield publisher to a test function
    - delete stream
    """
    stream = 'rbfly.integration.publisher.batch'
    ctx = _stream_publisher_create(rbfly_client, stream, rbs.PublisherBatchFast)
    async with ctx as publisher:
        yield publisher

@pta.fixture
async def stream_small_no_batch(rbfly_client: Client) -> StreamCtx:
    """
    Stream with a small number of messages, no batching.
    """
    stream = 'rbfly.integration.small.no-batch'
    ctx = _stream_populate(rbfly_client, stream, range(DEFAULT_STREAM_SMALL))
    async with ctx:
        yield await _stream_subscribe(rbfly_client, stream)

@pta.fixture
async def stream_small_batch(rbfly_client: Client) -> StreamCtx:
    """
    Stream with a small number of messages, batch of 4.
    """
    stream = STREAM_NAME_SMALL_BATCH
    ctx = _stream_populate_batch(
        rbfly_client, stream, range(DEFAULT_STREAM_SMALL), 4
    )
    async with ctx:
        yield await _stream_subscribe(rbfly_client, stream)

@pta.fixture
async def stream_large_no_batch(rbfly_client: Client) -> StreamCtx:
    """
    Stream with a large number of messages, no batching.
    """
    stream = 'rbfly.integration.large.no-batch'
    ctx = _stream_populate(rbfly_client, stream, range(DEFAULT_STREAM_LARGE))
    async with ctx:
        yield await _stream_subscribe(rbfly_client, stream)

@pta.fixture
async def stream_large_batch(rbfly_client: Client) -> StreamCtx:
    """
    Stream with a large number of messages, batch of 11.
    """
    stream = 'rbfly.integration.large.batch'
    ctx = _stream_populate_batch(
        rbfly_client, stream, range(DEFAULT_STREAM_LARGE), 11
    )
    async with ctx:
        yield (await _stream_subscribe(rbfly_client, stream))

async def stream_read(stream: Stream) -> AsyncIterator[int]:
    """
    Read data from a stream.
    """
    try:
        async for msg in stream.messages:
            assert isinstance(msg, int)
            yield msg
    except TimeoutError:
        pass

async def send_then_read(
    client: Client,
    suffix: str,
    publish_args: dict[str, tp.Any],
    subscribe_args: dict[str, tp.Any],
) -> list[int]:
    """
    Publish data into a stream, then read and return the values.

    :param client: RabbitMQ Streams client.
    :param suffix: Stream name suffix.
    :param publish_args: Arguments to pass to the publisher.
    :param subscribe_args: Arguments to pass to subscriber.
    """
    name = '{}.{}'.format(STREAM_NAME_SMALL_BATCH, suffix)
    ctx = _stream_populate_batch(
        # each batch has 10 items
        client, name, range(DEFAULT_STREAM_SMALL), 10, **publish_args,
    )
    stream = await _stream_subscribe(client, name, **subscribe_args)

    async with ctx:
        logger.info('reading data from stream: {}'.format(name))
        return [m async for m in stream_read(stream)]

@asynccontextmanager
async def _stream_populate(
        client: Client,
        stream: str,
        items: tp.Iterable[int],
) -> AsyncIterator[None]:
    """
    Populate a stream with messages, no batching.

    Actions:

    - create stream
    - populate it with messages
    - yield to a test function
    - delete stream
    """
    ctx = _stream_publisher_create(client, stream, rbs.Publisher)
    async with ctx as publisher:
        for msg in items:
            await publisher.send(msg)
        yield

@asynccontextmanager
async def _stream_populate_batch(
        client: Client,
        stream: str,
        items: StreamInput,
        batch_size: int,
        *,
        filter_extract: rbs.BloomFilterExtract | None=None,
) -> AsyncIterator[None]:
    """
    Populate a stream with messages in batches.

    Actions:

    - create stream
    - populate it with messages
    - yield to a test function
    - delete stream
    """
    ctx = _stream_publisher_create(
        client, stream, rbs.PublisherBatchFast, filter_extract=filter_extract
    )
    batches = partition(items, batch_size)
    async with ctx as publisher:
        for batch in batches:
            for msg in batch:
                publisher.batch(msg)
            await publisher.flush()

        logger.info('data sent to stream: {}'.format(stream))
        yield

async def _stream_subscribe(
        client: Client,
        stream: str,
        *,
        offset: rbs.Offset=rbs.Offset.offset(0),
        timeout: float=0.2,
        filter: rbs.MessageFilter | None=None,
) -> Stream:
    """
    Subscrie to a stream.
    """
    messages = client.subscribe(
        stream, offset=offset, filter=filter, timeout=timeout
    )
    return Stream(stream, messages)  # type: ignore

@asynccontextmanager
async def _stream_publisher_create(
        client: Client,
        stream: str,
        cls: type[T],
        *,
        filter_extract: rbs.BloomFilterExtract | None=None
) -> StreamPublisher[T]:
    """
    Create stream publisher.
    """
    await client.create_stream(stream)
    try:
        ctx = client.publisher(stream, cls=cls, filter_extract=filter_extract)
        async with ctx as publisher:
            yield publisher
    finally:
        await client.delete_stream(stream)

# vim: sw=4:et:ai
