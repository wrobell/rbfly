#
# rbfly - a library for RabbitMQ Streams using Python asyncio
#
# Copyright (C) 2021-2024 by Artur Wroblewski <wrobell@riseup.net>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

"""
Tests for RabbitMQ Streams integration on AMQP level.
"""

import logging
import os
import pika  # type: ignore[import-untyped]
from itertools import islice

from rbfly.amqp._message import decode_amqp

import pytest
from conftest import DEFAULT_STREAM_SMALL, Stream

logger = logging.getLogger(__name__)

@pytest.mark.asyncio
async def test_basic_consume(stream_small_no_batch: Stream) -> None:
    """
    Test consumption of AMQP messages with Pika library.
    """
    stream = stream_small_no_batch
    queue_args = {'x-stream-offset': 'first'}

    uri = os.environ.get(
        'RBFLY_TEST_AMQP_URI', 'amqp://guest:guest@localhost:5672/%2F'
    )
    params = pika.URLParameters(uri)
    connection = pika.BlockingConnection(params)
    channel = connection.channel()

    logger.info('number of messages to fetch: {}'.format(DEFAULT_STREAM_SMALL))
    channel.basic_qos(prefetch_count=DEFAULT_STREAM_SMALL)

    items = channel.consume(stream.name, arguments=queue_args)
    items = islice(items, DEFAULT_STREAM_SMALL)
    messages = (decode_amqp(item[2]) for item in items)
    result = [m.body for m in messages]
    assert result == list(range(DEFAULT_STREAM_SMALL))

# vim: sw=4:et:ai
